<?php
    
    $dbhost = "";
    $dbuser = "";
    $dbpass = "";
    $dbname = ""; 
    
    $homepaths = "";  //USE A / BEHIND!
    
    $dsn  = 'mysql:dbname='.$dbname.';host='.$dbhost.';charset=utf8';
    $user = $dbuser;
    $pass = $dbpass;
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_EMULATE_PREPARES => false
    ];
    $pdo = new PDO($dsn, $user, $pass, $options);
    
    //echo $_POST[ 'type' ];
    if(!empty($_GET)) {
        $type = $_GET[ 'type' ];
        switch( $type ) {
            case 'passwordCheck':
                $ret = checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]);
                echo json_encode( array( 'isPasswordCorrect' => $ret));
                break;
            case 'createUser':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]) ) {
                    echo json_encode( array( 'response' => createUser($pdo, $_GET[ 'newuser' ], $_GET[ 'newpasswd' ], $homepaths)));
                } else echo json_encode( array( 'response' => false ) );
                break;
            case 'deleteUser':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]) ) {
                    echo json_encode( array( 'response' => deleteUser($pdo, $_GET[ 'username' ])));
                } else echo json_encode( array( 'response' => false ) );
                break;
            case 'passwordChange':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'oldpasswd' ]) ) {
                    echo json_encode( array( 'response' => changePassword($pdo, $_GET[ 'user' ], $_GET[ 'newpasswd' ])));
                } else echo json_encode( array( 'response' => false ) );
                break;
            case 'getDir':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]) ) {
                    echo json_encode( array( 'response' => getDir($_GET[ 'user' ], $homepaths)));
                } else echo json_encode( array( 'response' => false ) );
                break;
            case 'delFile':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]) ) {
                    echo json_encode( array( 'response' => delFile( $_GET[ 'file' ], $homepaths, $_GET[ 'user' ] )));
                } else echo json_encode( array( 'response' => false ) );
                break;
            case 'downloadFile':
                if( checkPassword($pdo, $_GET[ 'user' ], $_GET[ 'passwd' ]) ) {
                    //echo json_encode( array( 'response' => 'PENIS'));
                    echo json_encode( array( 'response' => download( $_GET[ 'file' ], $homepaths, $_GET[ 'user' ] )));
                } else echo json_encode( array( 'response' => false ) );
                break;
        
            }
    }
    
    if(!empty($_POST)) {
        $type = $_POST[ 'type' ];
        if( $type == 'upload') {
            saveFile( $_POST[ 'file' ], $_POST[ 'filename' ],  $homepaths, $_POST[ 'user' ]);
            //echo json_encode( array( 'response' => $_POST[ 'file' ]));
            echo json_encode( array( 'response' => true ));
        } else echo json_encode( array( 'response' => "Function not defined") );
    }

    function createUser(PDO $pdo, $username, $password, $homepaths) {
        $qry = "INSERT INTO account (name, password) VALUES (?, ?)";
        $statement = $pdo->prepare( $qry );
        $exec = array( $username, $password );
        if( $statement->execute( $exec )) {
            print("mkdir " . $homepaths . $username);
            $i=shell_exec("mkdir " . $homepaths . $username);
            return true;
        }
        else return false;
    }
    
    function saveFile($file, $filename, $homepaths, $username) {
        $img_base = base64_decode( $file );
        file_put_contents($homepaths . $username . "/" . $filename, $img_base );
    }

    function download( $file, $homepath, $username ) {
        return base64_encode( file_get_contents( $homepath . $username . "/" . $file ) );
        //return "Test";
    }

    function delFile( $file, $homepath, $user ) {
       //if( 
        shell_exec("rm -r " . $homepath . $user . "/" . $file );// {
        return true;
       //} else return false;
    }
    
    function getDir($username, $homepaths) {
        $arr = array();
        $i = shell_exec("ls -l " . $homepaths . $username . " | wc -l");
        for($j = 1; $j <= $i-1; $j++) {
            $entry = shell_exec("ls -l " . $homepaths . $username ." | sed 's/   */ /g' | sed -n '1!p' | sed '$j!d'" );
            $parts = explode(" ", $entry);
            if( substr($parts[0], 0, 1) == 'd' ) $type = 'directory';
            else $type = 'file';
            $parts[0] = substr($parts[0], 1, -1);
            $parts[8] = substr($parts[8], 0, -1);
            $arr2 = array(
                'type'  => $type,
                'perms' => $parts[0], 
                'links' => $parts[1],
                'owner' => $parts[2],
                'group' => $parts[3],
                'size'  => $parts[4],
                'month' => $parts[5],
                'date'  => $parts[6],
                'time'  => $parts[7],
                'file'  => $parts[8]
            );
            array_push($arr, $arr2);
        }
        return $arr;
    }
    
    function deleteUser(PDO $pdo, $username) {
        $qry = "DELETE FROM account WHERE name = :name";
        $statement = $pdo->prepare( $qry );
        $statement->bindParam( ':name', $username );
        if( $statement->execute()) return true;
        else return false;
    }
    
    function changePassword(PDO $pdo, $username, $password) {
        $qry = "UPDATE account SET password = ? WHERE name = ?";
        $statement = $pdo->prepare( $qry );
        $exec = array( $password, $username );
        if( $statement->execute( $exec )) {
            return true;
        }
        else return false;
    }  
    
    function checkPassword(PDO $pdo, $username, $password) {
        createTables($pdo);
        $qry = "SELECT password FROM account WHERE name = :name";
        $statement = $pdo->prepare( $qry );
        $statement->bindParam( ':name', $username );
        $statement->execute();
        while( $acc = $statement->fetch() ) {
            if( $password == $acc->password ) return true;
            else return false;
        }
        return false;
    }
    
    function createTables(PDO $pdo) {
        $qry = "CREATE TABLE IF NOT EXISTS account (
            name varchar(32) NOT NULL PRIMARY KEY,
            password varchar(32) NOT NULL
            )";
        $statement = $pdo->prepare( $qry );
        $statement->execute();
        $qry = "INSERT INTO TABLE account (name, password) VALUES (?, ?)";
        $statement = $pdo->prepare( $qry );
        $exec = array( "admin", "d033e22ae348aeb5660fc2140aec35850c4da997" );
        if( $statement->execute( $exec) )
            return true;
        else return false;
    }
    
?>
