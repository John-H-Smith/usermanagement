from appJar import gui
import requests
import hashlib
import random
import os
import base64

global error_msg
error_msg = "Noch alles gut"

def defaultfill():
    for i in range(len(config_split)-1):
        if config_split[i] == "User:":
            global Name
            Name = config_split[i+1]
        if config_split[i] == "URL:":
            global Path
            Path = config_split[i+1]

if os.path.isfile('config.txt'):
    config_object = open(r"config.txt", "r+")
    config_lines = config_object.read()
    global config_split
    config_split = config_lines.split(" ")
    config_object.close()
    defaultfill()
else:
    config = open(r"config.txt", "w")
    config.write("User: Name URL: Path")
    config.close()
    config_object = open(r"config.txt", "r+")
    config_lines = config_object.read()
    #global config_split
    config_split = config_lines.split(" ")
    config.close()
    defaultfill()

app = gui("Connect", "400x400")

def connect():
    app.hide("Login Fenster")
    global URL
    if len(app.getEntry("IP")) > 1:
        URL = app.getEntry("IP")
    else:
        URL = config_split[3]
    global Username
    if len(app.getEntry("Login")) > 0:
        Username = app.getEntry("Login")
    else:
        Username = config_split[1]
    global passwd
    passwd = hashlib.sha3_256(app.getEntry("Passwort").encode('utf-8')).hexdigest()
    data_send = {'user': Username, 'passwd': passwd, 'type': 'passwordCheck'}
    r = requests.get(URL, data_send)
    if r.status_code == 200:
        inf = r.json()
        if inf['isPasswordCorrect']:
            app.showSubWindow("Menu")
            config = open("config.txt", "w")
            config_all = (config_split[0] + " " + Username + " " + config_split[2] + " " + URL)
            config.write(config_all)
            config.close()
            defaultfill()
        else:
            error_msg = ("Verbindung konnte nicht hergestellt werden.")
            app.setLabel("Login Fenster", str(error_msg))
    else:
        error_msg = ("Falsche IP?")
        app.setLabel("Login Fenster", str(error_msg))

def exit():
    app.stop()

def createuser():
    newpasswd = hashlib.sha3_256(rando.encode('utf-8')).hexdigest()
    data_send = {'newuser': app.getEntry("Username_new"), 'newpasswd': newpasswd, 'type': 'createUser', 'user': Username, 'passwd': passwd}
    r = requests.get(URL, data_send)
    if r.status_code == 200:
        inf = r.json()
        if inf['response']:
            app.hideSubWindow("createUser")
            app.showSubWindow("Menu")
        else:
            app.setLabel("Neuen User anlegen", "Fehler bei erstellung")

def backToMenu():
    app.hideAllSubWindows()
    app.showSubWindow("Menu")

def deluser():
    data_send = {'username': app.getEntry("Userdel"), 'type': 'deleteUser', 'user': Username, 'passwd': passwd}
    r = requests.get(URL, data_send)
    if r.status_code == 200:
        inf = r.json()
        if inf['response']:
            backToMenu()
        else:
            app.setLabel("User löschen", "Fehler bei Löschen")

def changepasswdW():
    app.showSubWindow("changePassword")
    app.hideSubWindow("Menu")

def send():
    data_send = {'user': Username, 'passwd': passwd, 'type': 'getDir'}
    r = requests.get(URL, data_send)
    inf = r.json()
    app.startSubWindow('Daten')
    app.setSize(400, 400)
    for i in range(0, len(inf['response'])):
        if (inf['response'][i]['type'] == 'file'):
            app.setFg("orange")
            app.addRadioButton("Datei", inf['response'][i]['file'])
        if (inf['response'][i]['type'] == 'directory'):
            app.setFg("blue")
            # app.addLabel("Datei", inf['response'][i]['file'])
            app.addRadioButton("Datei", inf['response'][i]['file'])
    app.addFileEntry("Upload")
    app.addButton("raufladen", upload)
    app.addButton("Löschen", delete)
    app.addButton("Download", download)
    app.addButton("Hauptmenü", mainmenu)
    app.stopSubWindow()
    app.showSubWindow("Daten")

def delete():
    data_send = {'user': "Phil", 'passwd': passwd, 'type': 'delFile', 'file': str(app.getRadioButton("Datei"))}
    r = requests.get(URL, data_send)
    inf = r.json()
    refresh()

def mainmenu():
    app.destroySubWindow("Daten")
    backToMenu()

def upload():
    passwd = hashlib.sha3_256("pass".encode('utf-8')).hexdigest()
    image = open(app.getEntry("Upload"), 'rb')
    image_64_encode = base64.encodebytes(image.read())
    filepath = app.getEntry("Upload")
    filename = filepath.split("/")

    data_send = {'user': "Phil", "passwd": passwd, "type": "upload", "file": image_64_encode, "filename": filename[-1]}
    r = requests.post(URL, data_send)
    inf = r.json()
    refresh()

def download():
    data_send = {'user': "Phil", 'passwd': passwd, 'type': 'downloadFile', "file": str(app.getRadioButton("Datei"))}
    r = requests.post(URL, data_send)
    inf = r.json()
    print(inf)
    data = open(str(app.getRadioButton("Datei")), 'wb')
    data.write(base64.decodebytes(inf['response']))
    #data.write(base64.b64decode(inf['response']))

def refresh():
    app.hideAllSubWindows()
    app.destroySubWindow("Daten")
    send()

def changepasswd():
    old = hashlib.sha3_256(app.getEntry("oldpass").encode('utf-8')).hexdigest()
    new = hashlib.sha3_256(app.getEntry("newpass").encode('utf-8')).hexdigest()
    data_send = {'user': Username, 'type': 'passwordChange', 'oldpasswd': old, 'newpasswd': new}
    r = requests.get(URL, data_send)
    if r.status_code == 200:
        inf = r.json()
        if inf['response']:
            print("Passwort geändert")
            backToMenu()
        else:
            app.setLabel("Passwort ändern", "Fehlerhafte eingabe")

def reloadW():
    app.destroySubWindow("createUser")
    app.startSubWindow("createUser")
    app.setSize(400, 400)
    app.addLabel("Neuen User anlegen")
    app.addLabelEntry("Username_new")
    global rando
    rando = ''.join(random.choice('0123456789ABCDEF') for i in range(8))
    app.addLabel(rando)
    app.addButton("Create", createuser)
    app.addButton("Exit_create", reloadW)
    app.setButton("Exit_create", "Exit")
    app.stopSubWindow()
    backToMenu()

def adduser():
    app.showSubWindow("createUser")
    app.hideSubWindow("Menu")

def deluserW():
    app.showSubWindow("delUserW")
    app.hideSubWindow("Menu")

def createApp():
    app.setIcon("icon.ico")
    app.showIcon(False)
    app.addLabel("Login Fenster")
    app.addLabelEntry("IP")
    app.setEntryDefault("IP", Path)
    app.addLabelEntry("Login")
    app.setEntryDefault("Login", Name)
    app.addSecretEntry("Passwort")
    app.addButton("Verlassen", exit)
    app.addButton("Connect", connect)

    app.startSubWindow("Menu")
    app.setSize(400, 400)
    app.addButton("User anlegen", adduser)
    app.addButton("User löschen", deluserW)
    app.addButton("Passwort ändern", changepasswdW)
    app.addButton("Verbindung trennen", exit)
    app.addButton("Eigne Daten", send)
    app.stopSubWindow()

    app.startSubWindow("createUser")
    app.setSize(400, 400)
    app.addLabel("Neuen User anlegen")
    app.addLabelEntry("Username_new")
    global rando
    rando = ''.join(random.choice('0123456789ABCDEF') for i in range(8))
    app.addLabel(rando)
    app.addButton("Create", createuser)
    app.addButton("Exit_create", reloadW)
    app.setButton("Exit_create", "Exit")
    app.stopSubWindow()

    app.startSubWindow("delUserW")
    app.setSize(400, 400)
    app.addLabel("User löschen")
    app.addLabelEntry("Userdel")
    app.addButton("del", deluser)
    app.addButton("Exit", backToMenu)
    app.stopSubWindow()

    app.startSubWindow("changePassword")
    app.setSize(400, 400)
    app.addLabel("Passwort ändern")
    app.addLabelSecretEntry("oldpass")
    app.addLabelSecretEntry("newpass")
    app.addButton("Change now", changepasswd)
    app.addButton("Exit_changep", backToMenu)
    app.setButton("Exit_changep", "Exit")
    app.stopSubWindow()

createApp()
app.go()
